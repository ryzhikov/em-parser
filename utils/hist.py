#!/usr/bin/env python3

import numpy as np
from matplotlib import pyplot as plt
import sys

column = 0
filename = sys.stdin

if len(sys.argv) > 1:
    filename = sys.argv[1]

    if len(sys.argv) > 2:
        column = int(sys.argv[2])

vals = np.loadtxt(filename, dtype=int, usecols=(column))
bins = np.unique(vals)

print(bins)

c,b,p = plt.hist(vals, bins=bins, normed=False )

plt.show()
