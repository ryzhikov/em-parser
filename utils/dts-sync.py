#!/usr/bin/env python3
"""
Sychronize timestamp deltas
with specified timestamp drift and jitter.

Input: 
    binary or text files with timestamp deltas.

Print n of events:
     * total (when at least two times in sync),
     * before T0 (first sync),
     * unsync between T0 and the last sync,
     * unsync after the last sync.
"""

import numpy as np
import sys
import argparse
from itertools import islice


def _slide(m1, m2=None ):
    """ All combinations of x,y from 0,0 to m1,m2
        ordered by max(x,y) ascending.
    """
    if m2 is None:
        m2 = m1

    m1 += 1
    m2 += 1

    same = min(m1, m2)

    for n in range(0, same):
        for a in range(n):
            yield a, n
        for a in range(n+1):
            yield n, a

    if m1 > m2:
        for n in range(same, m1):
            for a in range(m2):
                yield n, a
    
    elif m1 < m2:
        for n in range(same, m2):
            for a in range(m1):
                yield a, n


def _seq_cmp(a, ia, b, ib, num, drift = 0, jitter = 1):
    """ Compare num elements of two sequences.
        If false, return the index of the first non-matching element.

        Returns: (True, None) or (False, M),
          where M is the index of the first non-match.
    """
    seq_a = islice(a, ia, ia+num)
    seq_b = islice(b, ib, ib+num)

    if drift == 0:
        compare = lambda x,y : abs(-0 + x - y) <= jitter
    else:
        compare = lambda x,y: abs(-0 + x - y ) <= abs(x/drift) + jitter
    
    i = 0  
    for i, ab in enumerate(zip(seq_a, seq_b)):
        if not compare(*ab):
            return False, i
    
    if num > i+1:
        raise IndexError

    return True, None


def _find_match(length, a, b, drift=0, jitter=1):
    """ Find leftmost common substring 
        with at least specified length
        for iterable a and b.
    """
    na = len(a) - length
    nb = len(b) - length

    for ia, ib in _slide(na, nb):
        res, n_fail = _seq_cmp(a, ia, b, ib, length, drift=drift, jitter=jitter)
        if res:
            return ia, ib

    return None, None


def _sync_all(*dt, length=8, drift=0, jitter=3):
    """ Synchronize several sequences: find a start of the leftmost
     common fragment with at least specified length.
    """
    n = len(dt)
    items = tuple(range(0,n))

    if (n < 2):
        raise ValueError
    
    match = [0] * n
    m0 = m1 = 0
    done = False
    
    while not done:
        # Find a match between the first two sequences 
        m0, m1 = _find_match(
                    length,
                    dt[0][match[0]:],  # a[None:] is a[0:] 
                    dt[1][match[1]:],
                    drift, jitter)

        if m0 is None:  # can't find more matches
            return None
        
        match[0] += m0
        match[1] += m1
        done = True  # our best hopes
        
        # Check the rest
        for x in items[2:]:
            m0, mx = _find_match(
                        length,
                        dt[0][match[0]:],
                        dt[x][match[x]:],
                        drift, jitter)

            if m0 is None:  # no match between 0 and x
                return None

            elif m0  == 0:  # match the same sequence
                match[x] += mx
                continue
            
            else:  # match NOT the same sequence
                match[0] += m0
                match[x] += mx
                done = False
                break  # first need to recheck m0, m1
        
        if done:        
            return tuple(match)

    return None


def _offsets(*dt, length, drift, jitter):
    """ Calculate dt initial offsets.
    Expects no overflows!
    """
    items = range(len(dt))
    match = _sync_all(*dt, length=length, drift=drift, jitter=jitter)

    if match is None:
        return None
    
    return tuple(sum(dt[i][0 : match[i]]) for i in items)


def _iter_sync(dt, length=8, drift=0, jitter=3):
    """ Yield a tuple of the next elements from each dt,
         the others is None if element is not in sync with others.
    """
    n = len(dt)
    items = list(range(n))

    iter_dt = [iter(x) for x in dt]

    offsets = _offsets(*dt, length=length, drift=drift, jitter=jitter)

    accum = [-offsets[i] for i in items]
    sync_mask = [True] * n

    while any(sync_mask):
        var = [abs(a/drift) if a is not None else None for a in accum]
        min_ = min(a for a in accum if a is not None)
        sync_mask = [ (accum[i] - min_) <= jitter + var[i] if accum[i] is not None
                else None for i in items]

        yield tuple(accum[i] if sync_mask[i] else None for i in items)

        if all(sync_mask):
            accum = [0] * n
        
        for i in items:
            try:
                if sync_mask[i]:
                    accum[i] += next(iter_dt[i])
            except StopIteration:
                sync_mask[i] = None
                accum[i] = None


def common_start(*strings):
    """ Returns the longest common substring
        from the beginning of the `strings`
    """
    def _iter():
        for z in zip(*strings):
            if z.count(z[0]) == len(z):  # check all elements in `z` are the same
                yield z[0]
            else:
                return

    return ''.join(_iter())


def main():
# Two or more files
    parser = argparse.ArgumentParser(usage='%(prog)s file file [file ...]', description=__doc__)

    parser.add_argument('file1', type=argparse.FileType('r'), nargs=1,  
            metavar='file', help=argparse.SUPPRESS)

    parser.add_argument('file', type=argparse.FileType('r'), nargs='+',
            metavar='file')

    parser.add_argument('--text', action='store_true',
            help='input files are text instead of binary')

    parser.add_argument('--debug', action='store_true',
            help='print sync log')

    args = parser.parse_args()
    files = args.file1 + args.file

    commonfile = common_start(*[x.name for x in files])[-11:-1]


    opts = {
        "length": 8, 
        "jitter": 3,
        "drift": 2048,
    }

    dtype = np.dtype('<u4')
    data = []

    if args.text:  # text input
        for f in files:
            data.append(np.loadtxt(f, dtype=dtype, usecols=(0)))

    else:  # binary input
        for f in files:
            data.append(np.fromfile(f, dtype=dtype, count=-1))

    dt = [d.astype('i')[1:] for d in data]

    match = _sync_all(*(x[:200] for x in dt), **opts)
    

    if args.debug:
        print('match', match)
    
    if match is None:
        sys.exit(-1)


    match0_min, match0_max = match[0], match[0] + opts['length']

    #TODO: split matching ang sync functions
    
    count = 0
    backlog = []
    n = len(dt)
    count0 = 0

    for coinc in _iter_sync(dt, **opts):
        
        matched = True
        if n - coinc.count(None) >=2:  #at least two matches
            count +=1
        else:
            matched = False

        backlog.append(coinc)
        if args.debug:
            #FIXME: if coinc[0] is none?
            #FIXME: ts(+N), (-N)...

            ifirst, first = next(((i,x) for i,x in enumerate(coinc)
                                    if x is not None))
            items = []
            
            if coinc[0] is not None:
                count0 +=1
            
            if matched:
                items.append("{}".format(count))
            else:
                items.append("X")

            if count0 <= match0_max and count0 > match0_min:
                items[0] = items[0] + "<"
            
            items[0] = items[0].ljust(8)

            for i,c in enumerate(coinc):
                if c is None:
                    items.append("".rjust(12,"X"))
                elif i == ifirst:
                    items.append(str(c))
                elif c == first:
                    items.append("=")
                else:
                    items.append("{:+d}".format(c-first))
            
            items = [items[0]] + [i.rjust(12) for i in items[1:]]
            print(' '.join((i for i in items)))
            #print('\t'.join( map(str,(coinc[0], *((x - coinc[0]) if x is not None
            #and coinc[0] is not None else None
            #     for x in coinc[1:])))))
    
    before = 0
    b = iter(backlog)

    for x in b:
        if not any(_ is None for _ in x):
            break
        before +=1

    during = 0
    for x in b:
        if not all(x):
            during +=1

    after = 0
    for x in reversed(backlog):
        if all(x):
            break
        after +=1

    during -= after


    print(commonfile, count, before, during, after)





if __name__ == "__main__":
    main()

