#!/usr/bin/env python3
"""
Convert timestamps to timestamp deltas.
"""
import numpy as np
import sys
import argparse


def main():
    parser = argparse.ArgumentParser( description=__doc__)

    parser.add_argument('file', type=argparse.FileType('r'),  
            metavar='file')

    parser.add_argument('--input-text', action='store_true',
            help='input file is text, not binary')

    parser.add_argument('--text', action='store_true',
            help='output text instead of binary')

    args = parser.parse_args()

    dtype = np.dtype('<u4')
    data = None

    if args.input_text:  # text input
        data = np.loadtxt(args.file, dtype=dtype, usecols=(0))

    else:  # binary input
        data = np.fromfile(args.file, dtype=dtype, count=-1)

    data[1:] -= data[:-1] 
    
    if args.text:
        np.savetxt(sys.stdout.buffer, data, fmt='%d')
    else:
        sys.stdout.buffer.write(data.tobytes('C'))
        

if __name__ == "__main__":
    main()

