#!/usr/bin/env python3

import unittest
dts_sync = __import__("dts-sync")

class DtsSyncInternalsTest(unittest.TestCase):
    """ Tests for _internal routines. """
    
    def test_slide(self):
        slide = dts_sync._slide
        
        def check (*args):
            return  " ".join("{}{}".format(a,b) 
                            for a,b in slide(*args))

        m0 = 0; m1 = 1; m2 = 2; m3 = 3; m4 = 4
        mX = ""

        a43 = "00 01 10 11 02 12 20 21 22 03 13 23 30 31 32 33 40 41 42 43"
        a13 = "00 01 10 11 02 12 03 13"
        a3  = "00 01 10 11 02 12 20 21 22 03 13 23 30 31 32 33"
        a10 = "00 10"
        a0  = "00"

        self.assertEqual(a43, check(m4, m3))
        self.assertEqual(a13, check(m1, m3))
        self.assertEqual(a3, check(m3))
        self.assertEqual(a10, check(m1, m0))
        self.assertEqual(a0, check(m0))
        self.assertRaises(TypeError, check, m3,mX)


    def test_seq_cmp(self):
        import numpy as np
        check = dts_sync._seq_cmp
        
        a = [ 1,2,3,4,5,7 ]
        b = [ 0,2,3,4,5,6 ]
        n = np.arange(5,dtype='<u2')
        n_over = np.arange(-1,4,dtype='<u2')  # [65535, 0, 1, 2, 3]
        
        d1 = [100, 200, 300, 399]
        d2 = [ 98, 198, 303, 402]
        
        self.assertTupleEqual( check(a, 1, b, 1, 4, jitter = 0), (True, None))
        self.assertTupleEqual( check(a, 1, b, 1, 5, jitter = 0), (False, 4) )
        self.assertTupleEqual( check(a, 0, b, 1, 4, jitter = 1), (True, None) )
        self.assertRaises( IndexError,  check, a, 1, b, 1, 6 )  # num is out of the range
        self.assertTupleEqual( check(a, 2, n, 2, 3), (True, None))
        self.assertRaises( ValueError, check, a, -3, n, -3, 3)  # no negative inexes
        self.assertTupleEqual( check(n_over, 0, n, 0, 5, jitter=1), (False, 0))  # allow no overflow
        self.assertTupleEqual( check(d1, 1, d2, 1, 3, jitter=0, drift=100), (True, None)) 
        self.assertTupleEqual( check(d1, 0, d2, 0, 4, jitter=0, drift=100), (False, 0))
        

    def test_find_match(self):
        check = dts_sync._find_match
       
        z = [] 
        a = [1,2,3,4,5,6,7]
        b = [7,6,5,2,3,2,3,4]

        opts = {"drift":0, "jitter":0}

        self.assertTupleEqual( check(2, a, b, **opts), (1,3))
        self.assertTupleEqual( check(3, a, b, **opts), (1,5))
        self.assertTupleEqual( check(3, b, a, **opts), (5,1))
        self.assertTupleEqual( check(1, z, a, **opts), (None,None))
        self.assertTupleEqual( check(0, a, b, **opts), (0,0))
        self.assertTupleEqual( check(4, a, b[4:], **opts), (None,None))

    
    def test_sync_all(self):
        check = dts_sync._sync_all

        a = [1,2,3,4,5,6,7,8,9]
        b = [0,2,3,4,5,6,7,8,9]
        c = [2,3,4,6,7,8,9]

        x = [99,1,2,3,4,5]
        y = [3,4,5]

        opts = {"drift":0, "jitter":0}

        self.assertTupleEqual( check(a,b,c, length=2, **opts), (1,1,0))
        self.assertTupleEqual( check(a,b,c, length=3, **opts), (1,1,0))
        self.assertTupleEqual( check(a,b,c, length=4, **opts), (5,5,3))
        self.assertIsNone( check(a,b,c, length=5, **opts))
        self.assertRaises(ValueError, check, a, length=3, **opts)

        self.assertTupleEqual( check(x,y, length=3, **opts), (3, 0))
        

    def test_offsets(self):
        
        check = dts_sync._offsets

        a = [99,1,2,3,4,5]
        b = [3,4,5]

        self.assertTupleEqual( check(a,b, length=3, drift=0, jitter=0), (102,0))


    def test_iter_sync(self):
        
        check = dts_sync._iter_sync

        a = [1,2,3,256,5,6]
        b = [1,2,3,257,5,6]
        c = [2,256,5,6]

        opts = {"length":3, "drift":256, "jitter":0}

        for x in check((a,b,c),  **opts):
            print(x)














if __name__ == "__main__":
    unittest.main()
