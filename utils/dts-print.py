#!/usr/bin/env python3
"""
"""
import numpy as np
import sys
import argparse


def main():
# Two or more files
    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument('files', type=argparse.FileType('r'), nargs='+',
            metavar='file')

    parser.add_argument('--text', action='store_true',
            help='input files are text instead of binary')

    args = parser.parse_args()
    files = args.files
    dtype = np.dtype( [('dt', '<u4')])
    data = []

    if args.text:  # text input
        for f in files:
            data.append(np.loadtxt(f, dtype=np.dtype([('dt', '<u4')]), usecols=(0)))

    else:  # binary input
        for f in files:
            data.append(np.fromfile(f, dtype=dtype, count=-1))

    dt_all = [d['dt'].astype('i') for d in data]

    # output timemarks
    
    n = len(dt_all)
    items = range(n)

    iterators = [iter(x) for x in dt_all ]
    vals = [next(i) for i in iterators]

    while any(vals):
        print(vals)

        for i in items:
            try:
                vals[i]= next(iterators[i])
            except StopIteration:
                vals[i] = None

        

if __name__ == "__main__":
    main()

