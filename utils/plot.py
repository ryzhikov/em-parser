#!/usr/bin/env python3
"""
Plot data on timeline.

Data format:
<ts_utc> <col1> <col2> ... <coln>
"""

import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import datetime as dt

column = 1
filename = sys.stdin

if len(sys.argv) > 1:
    filename = sys.argv[1]

    if len(sys.argv) > 2:
        column = int(sys.argv[2])


ts, vals =  np.loadtxt(filename, dtype="uint, float", usecols=(0, column), unpack = True)

is_sorted = lambda a: np.all(a[:-1] <= a[1:])

if not is_sorted(ts):
    print("ts is not sorted")
    exit(1)

days = mdates.DayLocator()

dates=[dt.datetime.fromtimestamp(x) for x in ts]

vals_mean = [np.mean(vals)]*len(vals)

window_width = 100
cumsum = np.cumsum(np.insert(vals, 0, 0)) 
vals_ma = (cumsum[window_width:] - cumsum[:-window_width]) / window_width

print(len(dates), len(vals_ma[1:]))

plt.plot_date(dates[window_width:], vals[window_width:], 'bx', label="MWPC - EuroMISS")
plt.plot_date(dates[window_width:], vals_ma[1:], 'r',
        label="mov.avg (window=100")
plt.title('Difference in number of events')
plt.ylabel('')

plt.xticks( rotation=25 )
ax=plt.gca()
xfmt = mdates.DateFormatter('%m-%d')
ax.xaxis.set_major_formatter(xfmt)
ax.xaxis.set_major_locator(days)

plt.grid()
plt.legend()

plt.show()

