/**
 * Parse EuroMISS raw data for timestamps.
 *
 * Autor: Sergey Ryzhikov <sergey.ryzhikov@ihep.ru>, May 2018
 * Made for SPASCHARM experiment.
 */

#define _GNU_SOURCE

#include <argp.h>
#include <error.h>
#include <string.h> //strcmp
#include <stdlib.h> //strtol
#include <unistd.h> //dup
#include <sysexits.h>
#include <assert.h>
#include <sys/stat.h>

#include "parser-em5.h"
#include "gzopen.h"


const char *argp_program_version = "em-ts 2.0";
const char *argp_program_bug_address = "\"Sergey Ryzhikov\" <sergey.ryzhikov@ihep.ru>";
static char doc[] = "\nParse EuroMISS raw data file, " \
	"extract timestamps or timestamp deltas in binary or text form " \
	"(timestamps are 32-bit unsigned ints).\n";

static char args_doc[] = "[FILENAME]";

static struct argp_option options[] = { 
	{0,0,0,0, "If no FILENAME, waits for data in stdin." },
	{0,0,0,0, "Options:" },
	{ "outfile", 'o', "FILENAME", 0, "Use OUTFILE instead of stdout"},
	{ "delta", 'd', 0, 0, "Timestamp deltas (current-previous) instead of timestamp"},
	{ "text", 't', 0, 0, "Text output instead of binary"},
	{ "help", 'h', 0, OPTION_HIDDEN, ""},
	{ 0 } 
};

struct args {
	char *infile;
	char *outfile;
	bool delta;
	bool text;
};



static error_t 
parse_opt(int key, char *arg, struct argp_state *state)
{
	struct args *args = state->input;
	switch (key) {
		case 'd': args->delta = true; break;
		case 't': args->text = true; break;
		case 'o': args->outfile = arg; break;

		case 'h': 
			argp_state_help(state, stdout,
				ARGP_HELP_STD_HELP);
			break;

		case ARGP_KEY_NO_ARGS:
			// stdin by default;
			args->infile = "-";
			break;

		case ARGP_KEY_ARG: 
			if (state->arg_num == 0) {  // FILENAME
				args->infile = arg;
			}
			else {
				argp_usage(state); // catch a typo
			}
			break;

		default: return ARGP_ERR_UNKNOWN;
	}

	return 0;
}


static struct argp argp = { options, parse_opt, args_doc, doc };


int same_file(int fd1, int fd2) {
	/** Check if file descriptors point to the same file.
	 */
	struct stat stat1, stat2;
	if(fstat(fd1, &stat1) < 0) return -1;
	if(fstat(fd2, &stat2) < 0) return -1;
	return (stat1.st_dev == stat2.st_dev) && (stat1.st_ino == stat2.st_ino);
}

int em_ts( FILE * infile, FILE * outfile, FILE * errfile, struct args * args)
{
	emword wrd;
	struct parser_em5 parser = {{0}};
	enum parser_em5_ret ret;

	unsigned bytes;
	unsigned count = 0;
	unsigned ts;
	unsigned ts_prev = 0;
	unsigned outval;
	unsigned long long delta_summ = 0;

	while ((bytes = fread(&wrd, 1 /*count*/, sizeof(emword), infile)))
	{
		if (bytes != sizeof(emword)) {
			//ERR FILE_LEN_ODD 
			break;
		}
		
		ret = parser_em5_next(&parser, wrd);
		if (ret != RET_EVENT)
			continue;

		count += 1;

		if (!outfile) 
			continue;
		ts = parser.evt.ts;

		if (args->delta) {
			outval = ts - ts_prev;
			delta_summ += outval;
			ts_prev = ts;
		} else {
			outval = ts;	
		}

		if (args->text) {
			fprintf(outfile, "%u\n", outval);
		} else {
			fwrite(&outval, sizeof(outval), 1, outfile); 
		}
	}
	//last check
	assert(!args->delta || ts == delta_summ);  // deltas calculated correctly

	if ( !outfile || ! same_file(fileno(outfile), fileno(stdout)))  // if outfile redirected 
		printf("%u\n", count);  // print ts count
	return 0; 
}


int main(int argc, char *argv[])
{
	struct args args = {0};
	FILE * outfile = NULL;
	FILE * infile = NULL;
	int err;

	args.outfile = "-";  // default output to stdout
	argp_parse(&argp, argc, argv, 0, 0, &args);
	
	// outfile
	if ( !strcmp(args.outfile, "-") ) {  // output to stdout
		if (isatty(fileno(stdout)) && ! args.text) {  // binary printed on terminal
			errno = EPIPE;
			fprintf(stderr, "%s: Attempt to send binary output to terminal (use --outifle or --text for text output); %s\n",
                                        program_invocation_short_name, strerror (errno));
			exit(EX_USAGE);
		}
		else {
			outfile = fdopen(dup(fileno(stdout)), "wb"); // force binary output ...
					/*... for compatibility with lame operating systems */
		}
	} else if (!strcmp(args.outfile, "/dev/null")) {
		outfile = NULL;
	} else {
		outfile = fopen( args.outfile, "wb");  // rewrite outfile if exists
		if (outfile == NULL) {
			error(EX_IOERR, errno, "can't open file '%s'", args.outfile);
		}
	}
	
	infile = gzopen(args.infile);
	if (!infile) 
		error(EX_IOERR, errno, "can't open file '%s'", args.infile);	

	if (infile)
		err = em_ts(infile, outfile, stderr, &args);
	
	return err;
}
