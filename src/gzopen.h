#ifndef GZ_OPEN
#define GZ_OPEN

FILE * gzopen(const char * filename);

#endif  /* GZ_OPEN */
